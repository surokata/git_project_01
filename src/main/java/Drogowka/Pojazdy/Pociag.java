package Drogowka.Pojazdy;

public class Pociag extends Pojazd {

    private RodzajPociagu rodzajPociagu;
    private final static int PREDKOSCMAX = 100;
    private final int PREDKOSCMIN;

    public Pociag(RodzajPociagu rodzajPociagu) {

        this.rodzajPociagu = rodzajPociagu;
        setPredkosc(0);
        if (rodzajPociagu.equals(RodzajPociagu.TOWAROWY)) PREDKOSCMIN = 0;
        else {
            PREDKOSCMIN = -10;
        }
    }

    public void jedz() {
        if (getPredkosc() == 0) {
            System.out.println("Pociag rusza");
            przyspiesz();
        }
        System.out.println("Pociag jest w trakcie jazdy");
    }

    public void stoj() {
        setPredkosc(0);
        System.out.println("Pociag stoi.");
    }

    public void przyspiesz() {

        int predkoscPotencjalna = 0;

        if (getPredkosc() == PREDKOSCMAX) {
            System.out.println("Pociąg" + rodzajPociagu + "jedzie już z maksymalną prędkością.");
            return;
        }
        if (getPredkosc() < PREDKOSCMAX) {
            predkoscPotencjalna = getPredkosc() + 5;
        }

        if (predkoscPotencjalna > PREDKOSCMAX) {
            setPredkosc(PREDKOSCMAX);
        } else {
            setPredkosc(predkoscPotencjalna);
        }
        System.out.println("Pociąg " + rodzajPociagu + " przyspiesza do: " + getPredkosc());
    }

    public void zwolnij() {

        if (getPredkosc() == PREDKOSCMIN) {
            System.out.println("Pociąg " + rodzajPociagu + " już jedzie z minimalną prędkością.");
            return;
        }

        int predkoscPotencjalna;
        predkoscPotencjalna = getPredkosc() - 7;

        if (predkoscPotencjalna < PREDKOSCMIN) {
            setPredkosc(PREDKOSCMIN);
        } else {
            setPredkosc(predkoscPotencjalna);
        }
        System.out.println("Pociąg " + rodzajPociagu + " zwalnia do: " + getPredkosc());
    }

    public void trab() {
        setPredkosc((getPredkosc() * 9) / 10);
        System.out.println("Pociag " + rodzajPociagu + " trąbi i zwalnia do " + getPredkosc());
    }

    public RodzajPociagu getRodzajPociagu() {
        return rodzajPociagu;
    }

    public int getPREDKOSCMAX() {
        return PREDKOSCMAX;
    }

    public int getPREDKOSCMIN() {
        return PREDKOSCMIN;
    }
}