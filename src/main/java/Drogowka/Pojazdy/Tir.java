package Drogowka.Pojazdy;

public class Tir extends Pojazd {

    private final static int PREDKOSCMAX = 80;
    private final static int PREDKOSCMIN = -10;

    private String marka;
    private int liczbaTrabniec = 0;

    public Tir(String marka) {
        this.marka = marka;
        setPredkosc(0);
    }

    @Override
    public void jedz() {
        if (getPredkosc() == 0) {
            System.out.println("Tir" + marka + " rusza");
            przyspiesz();
        }
        System.out.println("Tir" + marka + " jest w trakcie jazdy");
    }

    @Override
    public void stoj() {
        setPredkosc(0);
        System.out.println("Tir " + marka + " stoi");
    }

    @Override
    public void przyspiesz() {

        int predkoscPotencjalna = 0;

        if (getPredkosc() == PREDKOSCMAX) {
            System.out.println("Tir" + marka + "jedzie już z maksymalną prędkością.");
            return;
        }
        if (getPredkosc() < PREDKOSCMAX) {
            predkoscPotencjalna = getPredkosc() + 1;
        }

        if (predkoscPotencjalna > PREDKOSCMAX) {
            setPredkosc(PREDKOSCMAX);
        } else {
            setPredkosc(predkoscPotencjalna);
        }
        System.out.println("Tir " + marka + " przyspiesza do: " + getPredkosc());
    }

    @Override
    public void zwolnij() {

        if (getPredkosc() == PREDKOSCMIN) {
            System.out.println("Tir " + marka + " już jedzie z minimalną prędkością.");
            return;
        }

        int predkoscPotencjalna;
        int ileZwalnia = (liczbaTrabniec > 2) ? 10 : liczbaTrabniec * 3 + 3;
        predkoscPotencjalna = getPredkosc() - ileZwalnia;

        if (predkoscPotencjalna < PREDKOSCMIN) {
            setPredkosc(PREDKOSCMIN);
        } else {
            setPredkosc(predkoscPotencjalna);
        }

        System.out.println("Tir " + marka + " zwalnia do: " + getPredkosc());

    }

    @Override
    public void trab() {

        System.out.println("Tir " + marka + " trąbi.");
        if (liczbaTrabniec < 3) liczbaTrabniec++;
    }

    @Override
    public int getPREDKOSCMAX() {
        return PREDKOSCMAX;
    }

    @Override
    public int getPREDKOSCMIN() {
        return PREDKOSCMIN;
    }

    public String getMarka() {
        return marka;
    }

    public int getLiczbaTrabniec() {
        return liczbaTrabniec;
    }
}
