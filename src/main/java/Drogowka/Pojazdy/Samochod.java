package Drogowka.Pojazdy;


public class Samochod extends Pojazd {

    private String marka;
    private final static int PREDKOSCMAX = 220;
    private final static int PREDKOSCMIN = -40;

    public Samochod(String marka) {
        setPredkosc(0);
        this.marka = marka;
    }

    @Override
    public void jedz() {
        if (getPredkosc() == 0) {
            System.out.println("Samochód " + marka + " rusza");
            przyspiesz();
        }
        System.out.println("Samochod " + marka + " jest w trakcie jazdy");
    }

    @Override
    public void stoj() {
        setPredkosc(0);
        System.out.println("Samochód " + marka + " stoi");
    }

    @Override
    public void przyspiesz() {

        int predkoscPotecjalna = 0;

        if (getPredkosc() == PREDKOSCMAX) {
            System.out.println("Samochód " + marka + " jedzie już z maksymalną prędkością.");
            return;
        }

        if (getPredkosc() <= 100) {
            predkoscPotecjalna = getPredkosc() + 2;
        }
        if (getPredkosc() > 100) {
            predkoscPotecjalna = getPredkosc() + 2 + (2 * zliczanieAwMarce());
        }
        if (predkoscPotecjalna > PREDKOSCMAX) {
            setPredkosc(PREDKOSCMAX);
        } else {
            setPredkosc(predkoscPotecjalna);
        }
        System.out.println("Samochód " + marka + " przyspiesza do: " + getPredkosc());
    }

    @Override
    public void zwolnij() {
        int predkoscPotencjalna;

        if (getPredkosc() == getPREDKOSCMIN()) System.out.println("Samochód " + marka + "jedzie z minimalną prędkością.");

        predkoscPotencjalna = getPredkosc() - 5;
        if (predkoscPotencjalna < PREDKOSCMIN) predkoscPotencjalna = PREDKOSCMIN;
            setPredkosc(predkoscPotencjalna);
            System.out.println("Samochód " + marka + " zwalnia do: " + getPredkosc());
    }

    @Override
    public void trab() {
        System.out.println("Samochód " + marka + " trąbi.");

    }

    private int zliczanieAwMarce() {

        //wersja 1 zliczania liter "A" i "a"
        int malea = marka.length() - marka.replace("a", "").length();
        int duzeA = marka.length() - marka.replace("A", "").length();
        return malea + duzeA;

    }

    public String getMarka() {
        return marka;
    }

    public int getPREDKOSCMAX() {
        return PREDKOSCMAX;
    }

    public int getPREDKOSCMIN() {
        return PREDKOSCMIN;
    }
}