package Drogowka.Pojazdy;

public abstract class Pojazd {
    public abstract void jedz();

    public abstract void stoj();

    public abstract void przyspiesz();

    public abstract void zwolnij();

    public abstract void trab();

    private int predkosc;

    public int getPredkosc() {
        return predkosc;
    }

    public abstract int getPREDKOSCMAX();

    public abstract int getPREDKOSCMIN();


    public void setPredkosc(int predkosc) {
        this.predkosc = predkosc;
    }


}
