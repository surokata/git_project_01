package Drogowka.Runner;

import Drogowka.Tunel.Tunel;
import Drogowka.Pojazdy.*;

public class TunnelRunner {
    public static void main(String[] args) {

        Tunel podwislostrada = new Tunel();
        Samochod s1 = new Samochod("Opel");
        s1.jedz();
        s1.przyspiesz();
        for (int i = 0; i < 200; i++) {
            s1.przyspiesz();
        }
        for (int i = 0; i < 200; i++) {
            s1.zwolnij();
        }
        s1.trab();
        s1.setPredkosc(56);

        Samochod s2 = new Samochod("Aaaa");
        s2.jedz();
        s2.przyspiesz();
        for (int i = 0; i < 200; i++) {
            s2.przyspiesz();
        }
        for (int i = 0; i < 200; i++) {
            s2.zwolnij();
        }
        s2.trab();
        s2.setPredkosc(56);

        Pociag p1 = new Pociag(RodzajPociagu.TOWAROWY);
        for (int i = 0; i < 50; i++) {
            p1.przyspiesz();
        }
        for (int i = 0; i < 10; i++) {
            p1.trab();
        }
        for (int i = 0; i < 100; i++) {
            p1.zwolnij();
        }
        p1.setPredkosc(64);

        Pociag p2 = new Pociag(RodzajPociagu.OSOBOWY);
        for (int i = 0; i < 50 ; i++) {
            p2.przyspiesz();
        }
        for (int i = 0; i <10 ; i++) {
            p2.trab();
        }
        for (int i = 0; i <100 ; i++) {
            p2.zwolnij();
        }
        p2.setPredkosc(90);

        Tir t1 = new Tir("Scania");
        for (int i = 0; i < 90; i++) {
            t1.przyspiesz();
        }
        for (int i = 0; i < 5; i++) {
            t1.trab();
            t1.zwolnij();
        }
        t1.setPredkosc(42);

        podwislostrada.wsadzWTunel(new Samochod("Alfa Romeo"));
        podwislostrada.pojazdyWTunelu.get(0).setPredkosc(92);
        podwislostrada.wsadzWTunel(new Samochod("Renault"));
        podwislostrada.pojazdyWTunelu.get(1).setPredkosc(64);
        podwislostrada.wsadzWTunel(new Samochod("Audi"));
        podwislostrada.pojazdyWTunelu.get(2).setPredkosc(150);
        podwislostrada.wsadzWTunel(new Tir("Man"));
        podwislostrada.pojazdyWTunelu.get(3).setPredkosc(-5);
        podwislostrada.wsadzWTunel(new Samochod("Alfa Romeo"));
        podwislostrada.pojazdyWTunelu.get(4).setPredkosc(92);
        podwislostrada.wsadzWTunel(s1);
        podwislostrada.wsadzWTunel(s2);
        podwislostrada.wsadzWTunel(p1);
        podwislostrada.wsadzWTunel(p2);
        podwislostrada.wsadzWTunel(t1);

        System.out.print("Pojazd w tunelu z minimalną prędkością jedzie z predkoscia: ");
        System.out.println(podwislostrada.pojazdZMinPredkoscia().getPredkosc());
        System.out.print("Pojazd w tunelu z maksymalną prędkością jedzie z predkoscia: ");
        System.out.println(podwislostrada.pojazdZMaxPredkoscia().getPredkosc());
        System.out.println("Liczba pojazdow w tunelu to: " + podwislostrada.liczPojazdy());
        System.out.println("Srednia predkosc pojazdow w tunelu to " + podwislostrada.sredniaPredkoscPojazdowWTunelu());
        podwislostrada.wypiszStatystyki();
        podwislostrada.usunPiratow();
        podwislostrada.wypiszStatystyki();

    }
}
