package Drogowka.Tunel;

import Drogowka.Pojazdy.*;

import java.util.ArrayList;

public class Tunel {

    private static final int MAKSYMALNAPREDKOSCDOPUSZCZALNA = 80;
    public ArrayList<Pojazd> pojazdyWTunelu = new ArrayList<Pojazd>();

    public Tunel() {
    }

    public int usunPiratow() {
        int liczbaPiratow = 0;
        for (int i = pojazdyWTunelu.size() - 1; i > -1; i--) {
            if (pojazdyWTunelu.get(i).getPredkosc() > MAKSYMALNAPREDKOSCDOPUSZCZALNA) {
                pojazdyWTunelu.remove(i);
                liczbaPiratow++;
            }
        }
        if (liczbaPiratow > 0) System.out.println("Piratów usunieto!");
        return liczbaPiratow;
    }

    public void wsadzWTunel(Pojazd pojazd) {
        pojazdyWTunelu.add(pojazd);
        System.out.println("Pojazd wjechał w tunel");
    }

    public int liczPojazdy() {
        return pojazdyWTunelu.size();
    }

    public double sredniaPredkoscPojazdowWTunelu() {
        int licznikSredniejPredkosci = 0;
        for (Pojazd p : pojazdyWTunelu) {
            licznikSredniejPredkosci = licznikSredniejPredkosci + p.getPredkosc();
        }
        return licznikSredniejPredkosci / pojazdyWTunelu.size();
    }

    public Pojazd pojazdZMinPredkoscia() {

        if (pojazdyWTunelu.size() == 0){
            System.out.println("Tunel nie zawiera pojazdów.");
            return null;
        }
        int indexListy = 0;
        int predkoscMin = pojazdyWTunelu.get(0).getPredkosc();
        for (Pojazd p : pojazdyWTunelu) {
            if (predkoscMin > p.getPredkosc()) {
                predkoscMin = p.getPredkosc();
                indexListy = pojazdyWTunelu.indexOf(p);
            }
        }
        return pojazdyWTunelu.get(indexListy);
    }

    public Pojazd pojazdZMaxPredkoscia() {

        if (pojazdyWTunelu.size() == 0){
            System.out.println("Tunel nie zawiera pojazdów.");
            return null;
        }
            int indexListy = 0;
            int predkoscMax = pojazdyWTunelu.get(0).getPredkosc();
            for (int i = 0; i < pojazdyWTunelu.size(); i++) {
                if (predkoscMax < pojazdyWTunelu.get(i).getPredkosc()) {
                    predkoscMax = pojazdyWTunelu.get(i).getPredkosc();
                    indexListy = i;
                }
            }
            //System.out.println("Pojazd w tunelu z maksymalną prędkością jedzie z predkoscia: ");
            return pojazdyWTunelu.get(indexListy);



    }

    public StatystykiTunelu statystyka() {
        return new StatystykiTunelu();
    }

    public void wypiszStatystyki() {
        statystyka().getMassage();
    }

    private class StatystykiTunelu {
        private int liczbaTirow = 0;
        private int liczbaPociagow = 0;
        private int liczbaSamochodow = 0;
        private int liczbaCofajacych = 0;
        private int liczbaPrzekraczajacychPredkosc = 0;

        public StatystykiTunelu() {
            liczStaty();
        }

        private void liczStaty() {
            for (Pojazd p : pojazdyWTunelu) {
                if (p.getPREDKOSCMAX() == 220) liczbaSamochodow++;
                if (p.getPREDKOSCMAX() == 80) liczbaTirow++;
                if (p.getPREDKOSCMAX() == 100) liczbaPociagow++;
                if (p.getPredkosc() < 0) liczbaCofajacych++;
                if (p.getPredkosc() > MAKSYMALNAPREDKOSCDOPUSZCZALNA) liczbaPrzekraczajacychPredkosc++;
            }
        }

        public void getMassage() {
            System.out.println("W tunelu jest " + liczbaSamochodow +
                    " samochodów, " + liczbaPociagow + " pociagów i " +
                    liczbaTirow + " tirów." + liczbaPrzekraczajacychPredkosc +
                    " pojazdów przekracza predkosc, a " + liczbaCofajacych + " cofa.");

        }

        public int getLiczbaTirow() {
            return liczbaTirow;
        }

        public int getLiczbaPociagow() {
            return liczbaPociagow;
        }

        public int getLiczbaSamochodow() {
            return liczbaSamochodow;
        }

        public int getLiczbaCofajacych() {
            return liczbaCofajacych;
        }

        public int getLiczbaPrzekraczajacychPredkosc() {
            return liczbaPrzekraczajacychPredkosc;
        }
    }
}


